﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace CoupaDownload.Models
{
    public class Invoice
    {
        [DataType(DataType.DateTime)]
        public Nullable<DateTime> gldatestart { get; set; }
        [DataType(DataType.DateTime)]
        public Nullable<DateTime> gldateend { get; set; }
        [Required(ErrorMessage = "Job number is required")]
        public int jobnumber { get; set; }
        public string invoicenumber { get; set; }
        public Nullable<int> vendor { get; set; }
        [DataType(DataType.DateTime)]
        public Nullable<DateTime> invoicedatestart { get; set; }
        [DataType(DataType.DateTime)]
        public Nullable<DateTime> invoicedateend { get; set; }
        public string costcode { get; set; }

    }
} 